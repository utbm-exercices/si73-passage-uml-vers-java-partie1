package tp.exo1;

public abstract class MyClass {
    private int attr1;
    protected long attr2;
    float attr3;

    public boolean myfct1(int a, String b) {
        // empty body
        return true; // stub
    }

    protected static void myfct2() {
        // empty body
    }

    public abstract void myfct3();

    public final void myfct4() {
        // empty body
    }
}
